# TinDog

## La seule application pour trouver de la chienne !

### Description :

TinDog est une application sur la base de Tinder mais qui permet d'y enregistrer votre chien ou votre chienne. Puis comme pour tinder vous avez une liste de canidés de l'autre sexe (pas de LGBTMachinTruc++ chez nos amis à quatre pattes).

Swipt vers la gauche si le partenaire potentiel est hideux, swipt droit si c'est un Hot Dog (jeux de mot inside !). Et s'il y a un match, vous pouvez discuter.

### Git

Vous trouverez le code source sur [ce dépot GitLab](https://gitlab.com/OpenClassRoom/TinDog 'TinDog on GitLab').

### Installation

Ce projet utilise [Cocoapod](https://cocoapods.org/) pour les dépendances. Ils vous faut donc l'installer puis ouvrir à la racine du répertoire et ensuite installer les dépandances :

`$ pod install`

Puis ouvrir non pas le dossier `.xcodeproj` mais `.xcodeworkspace` :

`$ open App.xcworkspace`

### Model de données & dashboard

Le backend utilise [ParseServer](http://parseplatform.org/) hosté sur une instance [EC2 d'AWS](https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/concepts.html) qui relie le tout à une base de donnée [MongoDB](https://www.mongodb.com/) hebergé par [MLab](https://mlab.com/).

Voir schéma de l'architecture :

![Architecture](./TinDog_ArchitectureSchema.pdf)

Pour découvrir le model de donnée du projet il vous suffit d'installer le [dashboard](https://github.com/parse-community/parse-dashboard) de Parse Server sur votre machine. Un fichier de configuration est déjà prêt et se trouve à la racine du projet. Il vous suffit donc de faire :

`$ parse-dashboard --config parse-dashboard-config.json`

Un onglet sur votre navigateur doit s'ouvrir avec l'outil avec comme URL http://localhost:4040.

### Test de charge

Pour le test de charge j'utilise [artillery.io](https://artillery.io/) qui est lui aussi à installer si vous voulez faire le test. Le script de test se trouve dans `./LoadTest/testAPI.yml`. Vous pouvez lancer le test de chez vous pour test. Mais attention, comme j'utilise pour la phase de développement les instances gratuites d'AWS et de MLab, les services se mettent en "sommeil" si pas utilisé depuis plusieurs heures. Donc relancer le test une seconde fois peut amener de meilleur résultat.

Le script de test dans l'état où il est laissé sur le dépot GitLab est le meilleur résultat que j'ai pu avois sur 60 sec de `stress test`. Soit environ 8 utilisateurs qui font un CRUD complet sur le champ `User` (Create/Read/Login/Update/Delete) par seconde. J'ai au final ~10 réponses (400/401) en erreur sur 2400 requêtes pour 480 nouveaux `Users` créés. Soit ~0,4% d'echec. Ce qui est acceptable.

Screenshot du dernier test :

![Screenshot](./LoadTest/ScreenShotLoadTest.png)

La solution en cas de forte affluence serait de simplement passer aussi bien AWS que MLab sur des tiers payant plus costaud en I/O. C'est l'avantage de travailler avec d'un côté du SAAS et de l'autre une base de donnée NoSQL pour la scalabilité de l'ensemble.

### Fin de service

Je metterai un terme à l'accès à l'API une fois le projet P3 validé.

#### Gil Felot.
