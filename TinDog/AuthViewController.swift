//
//  AuthViewController.swift
//  TinDog
//
//  Created by Gil Felot on 21/01/2018.
//  Copyright © 2018 Gil Felot. All rights reserved.
//

import UIKit
import Parse

class AuthViewController: UIViewController {

    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var repasswordInput: UITextField!
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var infoText: UILabel!
    @IBOutlet weak var switchButton: UIButton!
    @IBOutlet weak var errMessage: UILabel!
    
    var isLogin = true
    var fakeData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        repasswordInput.isHidden = true
        errMessage.isHidden = true
        
        // Uncomment to add fake data user (1/2 dog 1/2 bitch)
//        createFakeData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if PFUser.current() != nil && fakeData == false {
            checkNewUserSegue()
        }
    }
    
    @IBAction func authAction(_ sender: Any) {
        if usernameInput.text == "" {
            showErrorMessage(m: "username is required.")
        } else if isLogin {
            if let username = usernameInput.text {
                if let password = passwordInput.text {
                    PFUser.logInWithUsername(inBackground: username, password: password, block: { (user, e) in
                        if e != nil {
                            self.getErrorFromServer(errServeur: e, errMessage: "Log in fail !")
                        } else {
                            print("Ok User Login !")
                            if PFUser.current()?["isBitch"] != nil {
                                self.performSegue(withIdentifier: "directLogin", sender: nil)
                            } else {
                                self.performSegue(withIdentifier: "login", sender: nil)
                            }
                        }
                    })
                }
            }
        } else {
            let user = PFUser()
            user.username = usernameInput.text
            user.password = passwordInput.text
            
            if passwordInput.text == "" || repasswordInput.text == "" {
                showErrorMessage(m: "Password is empty")
            } else if !matchPassword() {
                showErrorMessage(m: "Passwords doesn't match")
            } else {
                user.signUpInBackground(block: { (s, e) in
                    if e != nil {
                        self.getErrorFromServer(errServeur: e, errMessage: "Sign up fail !")
                    } else {
                        print("Ok User Done !")
                        self.checkNewUserSegue()
                    }
                })
            }
        }
    }
    
    @IBAction func switchAction(_ sender: Any) {
        if isLogin {
            repasswordInput.isHidden = false
            authButton.setTitle("Sign up", for: .normal)
            infoText.text = "Already an account ?"
            infoText.sizeToFit()
            switchButton.setTitle("Log in", for: .normal)
        } else {
            repasswordInput.isHidden = true
            authButton.setTitle("Log in", for: .normal)
            infoText.text = "Want to Sign up ?"
            infoText.sizeToFit()
            switchButton.setTitle("Sign up", for: .normal)
        }
        isLogin = !isLogin
    }
    
    func showErrorMessage(m: String) {
        errMessage.text = m
        errMessage.isHidden = false
    }
    
    func matchPassword() -> Bool {
        if passwordInput.text == repasswordInput.text {
            return true
        }
        return false
    }
    
    func getErrorFromServer(errServeur: Error?, errMessage: String) {
        var errMess = errMessage
        if let error = errServeur as NSError? {
            if let detailError = error.userInfo["error"] as? String {
                errMess = detailError
            }
        }
        self.showErrorMessage(m: errMess)
    }
    
    func checkNewUserSegue() {
        if PFUser.current()?["isBitch"] != nil {
            performSegue(withIdentifier: "directLogin", sender: nil)
        } else {
            performSegue(withIdentifier: "login", sender: nil)
        }
    }
    
    
    // Load fake date in viewDidLoad
    func createFakeData() {
        
        fakeData = !fakeData
        
        let imageURLS = [
            "http://brostrick-prod.s3.amazonaws.com/wp-content/uploads/2015/10/05084149/dog-bat-man-memes-2016.png",
            "https://static.boredpanda.com/blog/wp-content/uploads/2016/02/funny-dog-nose-closeups-fb.png",
            "https://thechive.files.wordpress.com/2017/09/screen-shot-2017-09-17-at-10-14-47-pm.png?w=638&h=517",
            "http://www.fetchthebuzz.com/wp-content/uploads/2016/12/dog-smile-5.png",
            "http://www.pcdesktopbackgrounds.com/wp-content/uploads/2015/12/yourplaceorrmine-400x305.png",
            "https://vignette.wikia.nocookie.net/steven-universe/images/0/06/Funny_dog.png/revision/latest?cb=20150717003538",
            "http://www.pngmart.com/files/4/Boo-Dog-PNG-Clipart.png",
            "http://www.freeths.co.uk/blog/charity/wp-content/uploads/2013/10/dog-fun-dressing-up-canine-pet-animal-singular.png",
            "http://clipart.info/images/ccovers/1503688586funny-dog-with-glass-png.png",
            "http://www.pak101.com/funnypictures/Animals/2011/4/13/Funny_dog_halloween_fawek.png",
            "http://cdn.litlepups.net/2017/03/14/funny-dog-costume-photo-with-hot-dog-outfit-png-3-comments-hi-res.PNG",
            "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-9.png",
            "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-8.png",
            "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-7.png",
            "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-4.png",
            "https://www.puppypictures.org/main.php/d/77611-3/Funny+dog+costume+photo+with+hot+dog+outfit.PNG"
        ]
        
        var count = 0
        
        for imageUrl in imageURLS {
            if let url = URL(string: imageUrl) {
                if let data = try? Data(contentsOf: url) {
                    let imageFile = PFFile(name: "profilePic.png", data: data)
                    let user = PFUser()
                    user["image"] = imageFile
                    user.username = "Profil #" + String(count)
                    user.password = "pass"
                    user["isBitch"] = count % 2 == 0 ? true : false
                    user.signUpInBackground(block: { (success, error) in
                        if success {
                            print("User #", String(count), " created !!!")
                        }
                    })
                }
            }
            count += 1
        }
        
        fakeData = !fakeData
        PFUser.logOut()
    }
    
}
