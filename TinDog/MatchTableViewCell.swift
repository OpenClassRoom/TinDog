//
//  MatchTableViewCell.swift
//  TinDog
//
//  Created by Gil Felot on 05/02/2018.
//  Copyright © 2018 Gil Felot. All rights reserved.
//

import UIKit
import Parse

class MatchTableViewCell: UITableViewCell {

    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageInput: UITextField!
    
    var receiverID = ""
    
    @IBAction func messageSend(_ sender: Any) {
        let chat = PFObject(className: "Chat")
        chat["sender"] = PFUser.current()?.objectId
        chat["receiver"] = receiverID
        chat["message"] = messageInput.text
        
        chat.saveInBackground()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
