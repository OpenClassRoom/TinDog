//
//  MatchViewController.swift
//  TinDog
//
//  Created by Gil Felot on 05/02/2018.
//  Copyright © 2018 Gil Felot. All rights reserved.
//

import UIKit
import Parse

class MatchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var imagesProfil: [UIImage] = []
    var userIds: [String] = []
    var chat: [String] = []
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if let query = PFUser.query() {
            query.whereKey("acceptedDog", contains: PFUser.current()?.objectId)
            if let acceptedDog = PFUser.current()?["acceptedDog"] as? [String] {
                query.whereKey("objectId", containedIn: acceptedDog)
                query.findObjectsInBackground(block: { (objects, error) in
                    if let dogs = objects {
                        for dog in dogs {
                            if let oneDog = dog as? PFUser {
                                if let imageFile = oneDog["image"] as? PFFile {
                                    imageFile.getDataInBackground(block: { (data, error) in
                                        if let imageData = data { //
                                            if let img = UIImage(data: imageData) { //
                                                if let objectId = oneDog.objectId { //
                                                    let chatQuery = PFQuery(className: "Chat")
                                                    chatQuery.whereKey("receiver", equalTo: PFUser.current()?.objectId)
                                                    chatQuery.whereKey("sender", equalTo: oneDog.objectId!)
                                        
                                                    chatQuery.findObjectsInBackground(block: { (objects, error) in
                                                        var text = "No message in this chat yet !"
                                                        if let messages = objects {
                                                            for message in messages {
                                                                if let content = message["message"] as? String {
                                                                    text = content
                                                                }
                                                            }
                                                        }
                                                        self.imagesProfil.append(img)
                                                        self.userIds.append(objectId)
                                                        self.chat.append(text)
                                                        
                                                        self.tableView.reloadData()
                                                
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userIds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MatchTableViewCell {
            cell.profilImage.image = imagesProfil[indexPath.row]
            cell.receiverID = userIds[indexPath.row]
            cell.messageLabel.text = chat[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
}
