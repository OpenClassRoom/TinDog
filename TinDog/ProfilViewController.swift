//
//  ProfilViewController.swift
//  TinDog
//
//  Created by Gil Felot on 21/01/2018.
//  Copyright © 2018 Gil Felot. All rights reserved.
//

import UIKit
import Parse

class ProfilViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var genderSwitch: UISwitch!
    @IBOutlet weak var errMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errMessage.isHidden = true
        
        // Get logged dog if exist
        if let user = PFUser.current() {
            if let isBitch = user["isBitch"] as? Bool {
                genderSwitch.isOn = isBitch
            }
            // Get image from user if exist
            if let image = user["image"] as? PFFile {
                image.getDataInBackground(block: { (data, e) in
                    if e != nil {
                        // handle error
                    } else if let imageData = data {
                        if let i = UIImage(data: imageData) {
                            self.profilImage.image = i
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func uploadImageAction(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
//        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilImage.image = image
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateProfil(_ sender: Any) {
        PFUser.current()!["isBitch"] = genderSwitch.isOn
        
        if let image = profilImage.image {
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                PFUser.current()!["image"] = PFFile(name: "profile.png", data: data)
                
                print(PFUser.current()!)
                PFUser.current()?.saveInBackground(block: { (s, e) in
                    if e != nil {
                        print(e as Any)
                        self.getErrorFromServer(errServeur: e, errMessage: "Update Failed")
                    } else {
                        self.showSuccessMessage(m: "Info uploaded")
                        print("Data Saved !")
                        
                        self.performSegue(withIdentifier: "updatedProfile", sender: nil)
                    }
                })
            }
        }
    }
    
    func getErrorFromServer(errServeur: Error?, errMessage: String) {
        var errMess = errMessage
        if let error = errServeur as NSError? {
            if let detailError = error.userInfo["error"] as? String {
                errMess = detailError
            }
        }
        showErrorMessage(m: errMess)
    }
    
    func showSuccessMessage(m: String) {
        errMessage.text = m
        //        errMessage.textColor = UIColor.init(named: "MyGreen")
        errMessage.isHidden = false
    }
    
    func showErrorMessage(m: String) {
        errMessage.text = m
        errMessage.isHidden = false
    }
}
