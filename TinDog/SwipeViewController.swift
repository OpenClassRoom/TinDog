//
//  ViewController.swift
//  TinDog
//
//  Created by Gil Felot on 21/01/2018.
//  Copyright © 2018 Gil Felot. All rights reserved.
//

import UIKit
import Parse

class SwipeViewController: UIViewController {
    
    var dogDisplayed = ""
    
    @IBOutlet weak var swipeImageView: UIImageView!
    
    @IBOutlet weak var dogName: UILabel!
    
//    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDrag(gestureReco:)))
        swipeImageView.addGestureRecognizer(gesture)
        
        updateImage()
    }
    
    @objc func wasDrag(gestureReco: UIPanGestureRecognizer) {
        let labelPoint = gestureReco.translation(in: view)
        swipeImageView.center = CGPoint(x: view.bounds.width / 2 + labelPoint.x, y: view.bounds.height / 2 + labelPoint.y)
        
        let xFromCenter = view.bounds.width / 2 - swipeImageView.center.x
        
        // Add funny rotation animation to the label
        var rotation = CGAffineTransform(rotationAngle: xFromCenter / 200 * -1)
        let scale = min(100 / abs(xFromCenter), 1)
        var scaledAndRotated = rotation.scaledBy(x: scale, y: scale)
        swipeImageView.transform = scaledAndRotated
        
        // Check if swipe is right or left
        if gestureReco.state == .ended {
            
            var accepted = true
            
            // If swift left
            if swipeImageView.center.x < (view.bounds.width / 3) {
                print("GO Left Bitchies")
                accepted = false
                // If swift right
            } else if swipeImageView.center.x > (view.bounds.width * 2 / 3) {
                print("Hum right !")
                
            }
            
            if let user = PFUser.current() {
                if accepted && !dogDisplayed.isEmpty {
                    user.addUniqueObject(dogDisplayed, forKey: "acceptedDog")
                } else if !accepted && !dogDisplayed.isEmpty {
                    user.addUniqueObject(dogDisplayed, forKey: "rejectedDog")
                }
                user.saveInBackground(block: { (success, error) in
                    if success {
                        
                        self.updateImage()
                        
                    }
                })
            }
            
            
            
            // Put the label back to the initial position
            rotation = CGAffineTransform(rotationAngle: 0)
            scaledAndRotated = rotation.scaledBy(x: 1, y: 1)
            swipeImageView.transform = scaledAndRotated
            swipeImageView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
        }
    }
    
    func updateImage() {
        
        if let query = PFUser.query() {
            // Show spinner while Loading Images
            self.showSpinner()
            // Get user gender if exist as Bool
            if let dogType = PFUser.current()?["isBitch"] as? Bool {
                let dogLooking = dogType == true ? false : true
                query.whereKey("isBitch", equalTo: dogLooking)
            }
            
            var ignoredDog: [String] = []
            if let acceptedDog = PFUser.current()?["acceptedDog"] as? [String] {
                ignoredDog += acceptedDog
            }
            if let rejectedDog = PFUser.current()?["rejectedDog"] as? [String] {
                ignoredDog += rejectedDog
            }
            query.whereKey("objectId", notContainedIn: ignoredDog)
            
            
            // Limit the query to one answer
            query.limit = 1
            
            // Launch query async
            query.findObjectsInBackground { (objects, error) in
                
                // Get user Object if exist
                if let users = objects {
                    if (users.isEmpty) {
                        self.swipeImageView.isHidden = true
                        self.dogName.text = "No more dogs !"
                        self.hideSpinner()
                    } else {
                        for object in users {
                            
                            if let user = object as? PFUser {
                                // Get the dogId if exist
                                if let dogId = user.objectId {
                                    self.dogDisplayed = dogId
                                }
                                // Get image file if exist
                                if let imageFile = user["image"] as? PFFile {
                                    imageFile.getDataInBackground(block: { (data, error) in
                                        // Get image data if exist
                                        if let imageData = data {
                                            self.swipeImageView.image = UIImage(data: imageData)
                                            // hide spinner when UIImageView is populated
                                            self.hideSpinner()
                                        }
                                    })
                                    
                                }
                                // Get dog name if exist
                                if let name = user.username {
                                    self.dogName.text = name
                                }
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        PFUser.logOut()
        performSegue(withIdentifier: "logout", sender: nil)
    }
    
    
    // MARK: - SPINNER METHODS
    
    func showSpinner(){
        
        // Position Activity Indicator in the center of the main view
        spinner.center = view.center
        
        // If needed, you can prevent Acivity Indicator from hiding when stopAnimating() is called
        spinner.hidesWhenStopped = false
        
        // Start Activity Indicator
        spinner.startAnimating()
        
        //Show spinner
        view.addSubview(spinner)
    }
    
    func hideSpinner(){
        spinner.removeFromSuperview()
    }
}
